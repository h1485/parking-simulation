# Parkhaus Simulation

## Beschreibung

    Dieses Projekt dient dazu, ein Parkhaus zu simulieren.

## Features

    1) Einfahrt
        a) automatisch: Der Parkplatz wird automatisch zugewiesen, falls es einen freien gibt.
        b) manuell: Man gibt manuelle die Etage und Parkplatznummer.
    2) Ausfahrt
        a) Mit Kennzeichen: Man gibt das Kennzeichen des Fahrzeuges an. Es wird geprüft, 
           ob es in dem Parkhaus existiert. Wenn das der Fall ist, wird das auto ausgefahren.
        b) Mit Etage und Platznummer: Man gibt die Etage und Parkplatznummer an. Falls der Parkplatz besetzt ist, 
           wird er freigestellt.
    3) Einstellungen
       a) Man kann Etagen hinzufuegen/loeschen. Es muss aber mindestens immer eine Etage im Parkhaus existieren.
       b) Man kann bei jeder Etage die Anzahl an Parkplätzen ändern.

## ErweiterungsIdeen

    1) einen Event Logger einbauen, das man jederzeit abfragen kann.
    2) Verwalter muss ein passwort eingeben.
    3) Ticketsystem: Fahrer sollen wissen, wann die geparkt haben und für wie lange. (ggf. mit Preis)

