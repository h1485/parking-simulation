import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

class ParkingTest {

    Parking parking;
    Floor f0;
    Floor f1;
    Vehicle v1;
    Vehicle v2;

    @BeforeEach
    void setUp() {
        parking = new Parking(2);
        Floor f0 = new Floor(1);
        Floor f1 = new Floor(1);
        parking.setFloor(0, f0);
        parking.setFloor(1, f1);
        v1 = new Car("Car0");
        v2 = new Motorcycle("Motorcyle0");
    }

    @Test
    void getFloors() {
        Assertions.assertNotNull(parking.getFloors());
        Assertions.assertEquals(3, parking.getFloors().length);
    }

    @Test
    void setFloors() {
    }

    @Test
    void setFloor() {
        Floor f2 = new Floor(1);
        Assertions.assertNotEquals(parking.getFloors()[1], f2);
        parking.setFloor(1, f2);
        Assertions.assertEquals(parking.getFloors()[1], f2);
    }

    @Test
    void getSpotInfo() {
    }

    @Test
    void getParkingInfo() {
    }

    @Test
    void randomEnter() {
        Pair pair1 = parking.randomEnter(v1);
        Pair pair2 = parking.randomEnter(v2);

        Assertions.assertNotNull(pair1);
        Assertions.assertNotNull(pair2);

        Vehicle v3 = new Car("Car1");
        Pair pair3 = parking.randomEnter(v3); // parkhaus ist voll
        Assertions.assertNull(pair3);
    }

    @Test
    void enter() {
        Assertions.assertTrue(parking.enter(0, 0, v1));
        Assertions.assertFalse(parking.enter(0, 0, v2)); // is occupied
        Assertions.assertFalse(parking.enter(3, 0, v2)); // floor does not exist
        Assertions.assertFalse(parking.enter(1, 0, v1)); // v1 is already in the parking
    }

    @Test
    void leave() {
        parking.enter(0, 0, v1);
        Assertions.assertEquals(v1, parking.leave(0, 0));
        Assertions.assertNull(parking.leave(0, 0)); // spot is now free
    }

    @Test
    void getSpotByVehicleId() {
        Assertions.assertTrue(parking.enter(0, 0, v1));
        Assertions.assertEquals(0, parking.getSpotByVehicleId("Car0").getFirst());
        Assertions.assertEquals(0, parking.getSpotByVehicleId("Car0").getSecond());
        Assertions.assertThrows(NoSuchElementException.class, () -> parking.getSpotByVehicleId("Car10"));
    }

    @Test
    void addRemoveFloors() {
        parking.addRemoveFloors(3); // adding 3 floors
        Assertions.assertEquals(5, parking.getFloors().length);
        parking.addRemoveFloors(-2); // removing 2 floors
        Assertions.assertEquals(3, parking.getFloors().length);
        Assertions.assertThrows(IllegalArgumentException.class, () -> parking.addRemoveFloors(-3)); // at least 1 floor
    }
}
