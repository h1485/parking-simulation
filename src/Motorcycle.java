public class Motorcycle extends Vehicle {

    public Motorcycle(String id) {
        super(id);
        this.setType(VehicleType.MOTORCYCLE);
    }
}
