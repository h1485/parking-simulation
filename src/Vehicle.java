import java.util.HashSet;

public abstract class Vehicle {
    static HashSet<String> usedIds = new HashSet<>(); // track the used ids to prevent duplication
    private String id;
    private VehicleType type;

    public Vehicle(String id) {
        if (usedIds.contains(id)) {
            System.out.println("Vehicle with this ID already exists");
        } else {
            this.id = id;
            usedIds.add(id);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Fahrzeug Typ: " + this.type + "\n" +
            "Fahrzeug ID: " + this.id;
    }

}
