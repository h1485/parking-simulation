import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class App {
    private final String MENU_MESSAGE = "Tragen Sie die Nummer in den Klammern ein: ";
    private final String FLOOR_MESSAGE = "Waehlen Sie eine gueltige Etage aus: ";
    private final String SPOT_MESSAGE = "Waehlen Sie einen gueltigen Parkplatz aus: ";
    private int userType = -1;

    private Parking parking;

    public App(Parking parking) {
        this.parking = parking;
    }

    public void startApp() {
        displayMainMenu();
    }

    // get user Input
    private int getChoice(int numberChoices, String message) {
        Scanner scanner = new Scanner(System.in);
        int action = -1;
        while (action < 0 || action >= numberChoices) { // repeat until requirements are met
            System.out.print(message);
            while (true) {
                try {
                    action = scanner.nextInt();
                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Es sind nur Zahlen erlaubt! Versuchen Sie bitte nochmal!: ");
                }
            }
        }

        return action;
    }

    private void displayMainMenu() {
        seperateMenu();
        System.out.println("Sie Sind: ");
        System.out.println("Autofahrer (0)\nMotorradfahrer (1)\nVerwalter (2)\n");
        int action = getChoice(3, MENU_MESSAGE);

        if (action == 0 || action == 1) {
            userType = action;
            displayDriverMenu();
        } else if (action == 2) {
            displayAdminMenu();
        }
    }

    private void displayDriverMenu() {
        seperateMenu();
        System.out.println("Sie wollen: ");
        System.out.println("einfahren (0)\nausfahren (1)\nHauptmenue (2)\n");

        int action = getChoice(3, MENU_MESSAGE);
        if (action == 0) {
            displayEnterMenu();
        } else if (action == 1) {
            displayLeaveNextMenu();
        } else {
            displayMainMenu();
        }
    }

    private void displayEnterMenu() {
        seperateMenu();
        System.out.println("Wie wollen Sie den Parkplatz auswaehlen?");
        System.out.println("manuell (0)\nautomatisch (1)\nHauptmenue (2)");
        System.out.println("Tragen Sie die entsprechende Nummer ein: ");
        int action = getChoice(3, MENU_MESSAGE);
        if (action == 0) {
            displayManualMenu();
        } else if (action == 1) {
            displayAutomaticEnterMenu();
        }
    }

    private void displayLeaveMenu() {
        seperateMenu();
        System.out.println("ausfahren (0)\nHauptmenue (1)\n");
        int action = getChoice(3, MENU_MESSAGE);

        if (action == 0) {
            displayLeaveNextMenu();

        } else if (action == 1) {
            displayMainMenu();
        }
    }

    private void displayLeaveNextMenu() {
        seperateMenu();
        System.out.println("Danke, dass Sie in unserem Parkhaus geparkt haben!");
        System.out.println("Waehlen Sie eine Ausfahrtmethode aus: ");
        System.out.println("mit Kennzeichen (0)\nmit Etage und Parkplatz (1)");
        int action = getChoice(3, MENU_MESSAGE);

        if (action == 0) {
            System.out.println("Geben Sie das Kennzeichen an: ");
            Scanner scanner = new Scanner(System.in);
            String id = scanner.next();

            try {
                Pair info = parking.getSpotByVehicleId(id);
                parking.getFloors()[info.getFirst()].getParkingSpots()[info.getSecond()] = null;
                System.out.println("Fahrzeug mit dem Kennzeichen \"" + id + "\" hat das Parkhaus verlassen");
            } catch (NoSuchElementException e) {
                System.out.println("Es existiert kein Fahrzeug mit dem eingegebenen Kennzeichen in unserem Parkhaus!");
            }
        } else if (action == 1) {
            int floor = getChoice(parking.getFloors().length, FLOOR_MESSAGE);
            int spot = getChoice(parking.getFloors()[floor].getParkingSpots().length, SPOT_MESSAGE);

            Vehicle v = parking.leave(floor, spot);

            if (v == null) {
                System.out.println("Der eingebebene Parkplatz ist nicht besetzt!");
            } else {
                System.out.println("Fahrzeug mit dem Kennzeichen \"" + v.getId() + "\" hat das Parkhaus verlassen");
            }
        }

        displayMainMenu();
    }

    private void displayAutomaticEnterMenu() {
        seperateMenu();

        Vehicle v;

        if (userType == 0) {
            v = new Car("Car" + Vehicle.usedIds.size());
        } else {
            v = new Motorcycle("Motorcyle" + Vehicle.usedIds.size());
        }
        Pair result = parking.randomEnter(v);
        if (result != null) {
            printInfo(v.getId(), result.getFirst(), result.getSecond());
            displayLeaveMenu();
        } else {
            System.out.println("Momentan sind alle Parkplaetze besetzt! Kommen Sie bitte spaeter nochmal.");
            displayMainMenu();
        }
    }

    private void displayManualMenu() {
        seperateMenu();
        int floor = getChoice(parking.getFloors().length, FLOOR_MESSAGE);
        if (parking.getFloors()[floor].getFreeSpots() <= 0) {
            System.out.println("Diese Etage ist leider voll! Kommen Sie bitte spaeter nochmal");
            displayMainMenu();
        } else {
            Vehicle v;

            if (userType == 0) {
                v = new Car("Car" + Vehicle.usedIds.size());
            } else {
                v = new Motorcycle("Motorcyle" + Vehicle.usedIds.size());
            }

            int spot = getChoice(parking.getFloors()[floor].getParkingSpots().length, SPOT_MESSAGE);
            boolean result = parking.enter(floor, spot, v);
            if (!result) {
                System.out.println("Dieser Parkplatz ist leider besetzt! Kommen Sie bitte spaeter nochmal");
                displayMainMenu();
            } else {
                printInfo(v.getId(), floor, spot);
                displayLeaveMenu();
            }
        }

    }

    private void displayAdminMenu() {
        seperateMenu();

        System.out.println("Sie wollen: ");
        System.out.println("""
            Etagen hinzufuegen/loeschen (0)
            Parkplaetze hinzufuegen/loeschen (1)
            Allgemeine Informationen ueber das Parkhaus (2)
            Informationen ueber ein Fahrzeug (3)
            Hauptmenue (4)
            Exit (5)"""
        );

        int action = getChoice(6, MENU_MESSAGE);

        switch (action) {
            case 0:
                displayAddRemoveFloorMenu();
                System.out.println();
                displayAdminMenu();
                break;
            case 1:
                displayAddRemoveSpotMenu();
                System.out.println();
                displayAdminMenu();
                break;
            case 2:
                System.out.println(parking.getParkingInfo());
                System.out.println();
                displayAdminMenu();
                break;
            case 3:
                displayVehicleInfoMenu();
                System.out.println();
                displayAdminMenu();
                break;
            case 4:
                System.out.println();
                displayMainMenu();
            case 5:
                exit();
            default:
                System.out.println();
                displayMainMenu();
        }
    }

    private void printInfo(String id, int floor, int spot) {
        System.out.printf("Kennzeichen: \"%s\"\n", id);
        System.out.printf("Etage: %d, Parkplatz: %d\n\n", floor, spot);
    }

    private void exit() {
        System.exit(0);
    }

    private void displayAddRemoveFloorMenu() {
        System.out.println("Geben Sie an, wie viele Etagen Sie hinzufuegen möchten: ");
        System.out.println("(Negative Zahl fürs Loeschen)");
        Scanner scanner = new Scanner(System.in);
        int amount = 0;
        while (true) {
            try {
                amount = scanner.nextInt();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Bitte Geben Sie eine Zahl ein");
            }
        }

        try {
            parking.addRemoveFloors(amount);
            System.out.println(parking.getParkingInfo());
        } catch (IllegalArgumentException e) {
            System.out.println("Das Parkhaus muss mindestens eine Etage haben!");
        }
    }

    private void displayAddRemoveSpotMenu() {
        int floor = getChoice(parking.getFloors().length, FLOOR_MESSAGE);
        System.out.println("Geben Sie die neue Anzahl an Parkplaetzen an: ");
        Scanner scanner = new Scanner(System.in);
        int quantity = 0;
        while (true) {
            try {
                quantity = scanner.nextInt();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Bitte Geben Sie eine Zahl ein");
            }
        }

        try {
            parking.getFloors()[floor].setTotalParkingSpots(quantity);
        } catch (IllegalArgumentException e) {
            System.out.println("Eine Etage darf keine Negative Anzahl an Parkplaetzen haben!");
        }

    }

    private void displayVehicleInfoMenu() {
        System.out.println("Geben Sie das Kennzeichen des Fahrzeuges an: ");
        Scanner scanner = new Scanner(System.in);
        String id = scanner.next();

        try {
            Pair info = parking.getSpotByVehicleId(id);
            System.out.println(info);
        } catch (NoSuchElementException e) {
            System.out.println("Es existiert kein Fahrzeug mit diesem Kennzeichen in unserem Parkhause!");
        }
    }

    private void seperateMenu() {
        System.out.println("---------------------------------------------------------\n");
    }

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }
}
