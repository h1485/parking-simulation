import java.util.Random;

public class Floor {
    private Vehicle[] parkingSpots;
    private int totalParkingSpots;

    public Floor(int totalParkingSpots) {
        this.parkingSpots = new Vehicle[totalParkingSpots];
        this.totalParkingSpots = totalParkingSpots;
    }

    public Vehicle[] getParkingSpots() {
        return parkingSpots;
    }

    public void setParkingSpots(Vehicle[] parkingSpots) {
        this.parkingSpots = parkingSpots;
    }

    public int getTotalParkingSpots() {
        return totalParkingSpots;
    }

    public void setTotalParkingSpots(int newTotalParkingSpots) throws IllegalArgumentException {
        if (newTotalParkingSpots < 0) throw new IllegalArgumentException();
        Vehicle[] temp = new Vehicle[newTotalParkingSpots];
        System.arraycopy(parkingSpots, 0, temp, 0, Math.min(newTotalParkingSpots, totalParkingSpots));
        this.parkingSpots = temp;
        this.totalParkingSpots = newTotalParkingSpots;
    }

    public int getFreeSpots() {
        int count = 0;
        for (int i = 0; i < totalParkingSpots; i++) {
            if (parkingSpots[i] == null) {
                count++;
            }
        }

        return count;
    }

    public int getOccupiedSpots() {
        return totalParkingSpots - this.getFreeSpots();
    }

    public String getSpotInfo(int spotNumber) {
        return this.parkingSpots[spotNumber].toString();
    }

    public int randomEnter(Vehicle v) {
        if (getFreeSpots() == 0) {
            System.out.println("Alle Parkplätze sind besetzt!");
            return -1;
        }

        int random = new Random().nextInt(totalParkingSpots);
        while (parkingSpots[random] != null) {
            random = new Random().nextInt(totalParkingSpots);
        }

        this.parkingSpots[random] = v;

        return random;
    }

    public boolean enter(int spot, Vehicle v) {
        if (spot >= totalParkingSpots || spot < 0) {
            System.out.println("Ungültige Eingabe");
            return false;
        }

        if (parkingSpots[spot] != null) {
            System.out.println("Parkplatz ist besetzt!");
            return false;
        }

        parkingSpots[spot] = v;
        return true;
    }

    public Vehicle leave(int spot) {
        if (this.parkingSpots[spot] == null) {
            return null;
        } else {
            Vehicle temp = this.parkingSpots[spot];
            this.parkingSpots[spot] = null;
            return temp;
        }
    }
}
