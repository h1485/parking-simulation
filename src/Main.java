public class Main {
    public static void main(String[] args) {
        Parking p = Parking.generateParking();
        App app = new App(p);
        app.startApp();
    }
}
