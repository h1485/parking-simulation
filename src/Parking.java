import java.util.Arrays;
import java.util.NoSuchElementException;

public class Parking {
    private Floor[] floors;

    public Parking(int initialNumberFloors) {
        this.floors = new Floor[initialNumberFloors];
    }

    // generates a Parking Object
    public static Parking generateParking() {
        Parking parking = new Parking(1);
        Floor f0 = new Floor(10);
        parking.setFloor(0, f0);

        return parking;
    }

    public Floor[] getFloors() {
        return floors;
    }

    public void setFloors(Floor[] floors) {
        this.floors = floors;
    }

    // set floor at specific index
    public void setFloor(int floorIndex, Floor floor) {
        this.floors[floorIndex] = floor;
    }

    // shows the status of a specific spot (free or occupied)
    public String getSpotInfo(int floorNumber, int spotNumber) {
        if (floorNumber >= floors.length) return "Etage existiert nicht!";
        if (spotNumber >= floors[floorNumber].getTotalParkingSpots()) return "Parkplatz existiert nicht!";

        String temp = "Etage: " + floorNumber + "\n" +
            "Parkingplatz: " + spotNumber + "\n";

        if (floors[floorNumber].getParkingSpots()[spotNumber] == null) {
            temp += "Status: frei";
            return temp;
        } else {
            return "Etage: " + floorNumber + "\n" +
                "Parkingplatz: " + spotNumber + "\n" +
                "Status: besetzt" + "\n" +
                floors[floorNumber].getSpotInfo(spotNumber);
        }
    }

    // show information about floors and free parking spots
    public String getParkingInfo() {

        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < floors.length; i++) {
            temp.append("Etage ")
                .append(i)
                .append(": ")
                .append(floors[i].getFreeSpots())
                .append("/")
                .append(floors[i].getTotalParkingSpots())
                .append(" frei").append("\n");
        }
        System.out.println("");
        int totalParkingSpots = Arrays.stream(floors)
            .map(Floor::getTotalParkingSpots)
            .reduce(0, Integer::sum);

        int totalFreeParkingSpots = Arrays.stream(floors)
            .map(Floor::getFreeSpots)
            .reduce(0, Integer::sum);

        temp.append("total: ")
            .append(this.floors.length)
            .append(" Etage(n), Parkplaetze: ")
            .append(totalFreeParkingSpots)
            .append("/")
            .append(totalParkingSpots)
            .append(" Frei");
        return temp.toString();
    }

    // assign a parking spot automatically if there is a free one
    // gives back a pair (floor number, parking spot number) if successful else null
    public Pair randomEnter(Vehicle v) {
        try {
            getSpotByVehicleId(v.getId());
            System.out.println("Es existiert schon ein Fahrzeug mit diesem Kennzeichen im Parkhaus!");
        } catch (NoSuchElementException e) {
            for (int i = 0; i < floors.length; i++) {
                if (floors[i].getFreeSpots() > 0) {
                    return new Pair(i, floors[i].randomEnter(v));
                }
            }
        }

        return null;
    }

    // assign parking spot manually
    public boolean enter(int floor, int spot, Vehicle v) {
        if (floor >= this.floors.length) return false; // alternativ throw IllegalArgumentException
        try {
            getSpotByVehicleId(v.getId());
            System.out.println("Es existiert schon ein Fahrzeug mit diesem Kennzeichen im Parkhaus!");
            return false;
        } catch (NoSuchElementException e) {
            return this.floors[floor].enter(spot, v);
        }
    }

    // leave parking
    public Vehicle leave(int floor, int spot) {
        if (floor >= this.floors.length) return null;
        return this.floors[floor].leave(spot);
    }

    // returns the position of the vehicle if it exists
    public Pair getSpotByVehicleId(String id) throws NoSuchElementException {
        for (int i = 0; i < this.floors.length; i++) {
            for (int j = 0; j < this.floors[i].getTotalParkingSpots(); j++) {
                Vehicle tempVehicle = this.floors[i].getParkingSpots()[j];
                if (tempVehicle != null && tempVehicle.getId().equals(id)) {
                    return new Pair(i, j);
                }
            }
        }

        throw new NoSuchElementException();
    }

    // add or remove floors by a specific amount
    public void addRemoveFloors(int amount) throws IllegalArgumentException {
        if (this.floors.length + amount < 1) throw new IllegalArgumentException();
        if (amount == 0) return;

        Floor[] tempFloors = new Floor[this.floors.length + amount];
        System.arraycopy( // copy old floors to the new one
            this.floors,
            0,
            tempFloors,
            0,
            Math.min(this.floors.length, tempFloors.length)
        );

        for (int i = Math.min(this.floors.length, tempFloors.length); i < tempFloors.length; i++) {
            tempFloors[i] = new Floor(10); // initialize the new floors
        }

        this.floors = tempFloors; // update the floors
    }

}
